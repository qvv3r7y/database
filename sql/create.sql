drop table "18207_MATEYUK".ACCOUNTING_INSTRUMENTS CASCADE CONSTRAINTS;
drop table "18207_MATEYUK".INSTRUMENTS CASCADE CONSTRAINTS;
drop table "18207_MATEYUK".CONTRACT_STAGES CASCADE CONSTRAINTS;
drop table "18207_MATEYUK".SUBCONTRACTORS CASCADE CONSTRAINTS;
drop table "18207_MATEYUK".PERFORMERS CASCADE CONSTRAINTS;
drop table "18207_MATEYUK".PROJECTS CASCADE CONSTRAINTS;
drop table "18207_MATEYUK".CONTRACTS CASCADE CONSTRAINTS;
drop table "18207_MATEYUK".CLIENTS CASCADE CONSTRAINTS;
drop table "18207_MATEYUK".WORKERS_ENGINEER CASCADE CONSTRAINTS;
drop table "18207_MATEYUK".WORKERS_MANAGER CASCADE CONSTRAINTS;
drop table "18207_MATEYUK".WORKERS CASCADE CONSTRAINTS;
drop table "18207_MATEYUK".POSITIONS CASCADE CONSTRAINTS;
drop table "18207_MATEYUK".DEPARTMENTS CASCADE CONSTRAINTS;


CREATE TABLE "18207_MATEYUK".departments
(
    department_id   NUMBER(6) PRIMARY KEY,
    department_name VARCHAR2(50) NOT NULL,
    chief_id        NUMBER(6)    NOT NULL
);

CREATE TABLE "18207_MATEYUK".positions
(
    position_id    NUMBER(6) PRIMARY KEY,
    position_title VARCHAR2(50) NOT NULL
);

CREATE TABLE "18207_MATEYUK".workers
(
    workers_id    NUMBER(6) PRIMARY KEY,
    first_name    VARCHAR2(50),
    last_name     VARCHAR2(50) NOT NULL,
    phone_number  VARCHAR2(20) UNIQUE,
    date_of_birth DATE         NOT NULL,
    position_id   NUMBER(6)    NOT NULL,
    salary        NUMBER(8, 2),
    department_id NUMBER(6),
    type		  VARCHAR2(50) NOT NULL,
    CONSTRAINT emp_salary_min
        CHECK (salary > 12500),
    CONSTRAINT birthdayConstraint
        CHECK (date_of_birth >= '01-jan-1900'),
    CONSTRAINT emp_job_fk
        FOREIGN KEY (position_id)
            REFERENCES positions,
    CONSTRAINT emp_dept_fk
        FOREIGN KEY (department_id)
            REFERENCES departments
);

CREATE TABLE "18207_MATEYUK".workers_engineer
(
    workers_id      NUMBER(6) PRIMARY KEY,
    academic_degree VARCHAR2(50) NULL,
    experience      NUMBER(2)    NOT NULL,
    number_patents  NUMBER(3)    NOT NULL,
    specialization  VARCHAR2(50) NULL,
    FOREIGN KEY (workers_id)
        REFERENCES workers (workers_id)
);

CREATE TABLE "18207_MATEYUK".workers_manager
(
    workers_id           NUMBER(6) PRIMARY KEY,
    efficiency           NUMBER(2, 1) NOT NULL,
    mentoring_experience NUMBER(2)    NOT NULL,
    access_level         NUMBER(1)    NOT NULL,
    FOREIGN KEY (workers_id)
        REFERENCES workers (workers_id)
);

CREATE TABLE "18207_MATEYUK".clients
(
    client_id number(6) PRIMARY KEY,
    company   varchar2(100) NOT NULL,
    address   varchar2(100) NOT NULL,
    person    varchar2(100) NOT NULL,
    phone     varchar2(20)
);

CREATE TABLE "18207_MATEYUK".contracts
(
    contract_id  number(6) PRIMARY KEY,
    bank_details number(20),
    conclusions  date NOT NULL,
    begin        date NOT NULL,
    finish       date,
    client_id    number(6),
    manager_id   number(6),
    CHECK (conclusions <= begin),
    CHECK ( finish is null or finish > begin),
    CONSTRAINT con_chief_fk
        FOREIGN KEY (manager_id)
            REFERENCES workers (workers_id),
    CONSTRAINT con_id_fk
        FOREIGN KEY (client_id)
            REFERENCES clients
);

CREATE TABLE "18207_MATEYUK".projects
(
    project_id number(6)     NOT NULL UNIQUE,
    title      varchar2(120) NOT NULL,
    abbr       char(20) PRIMARY KEY,
    client_id  number(6),
    manager_id number(6),
    begin      date          NOT NULL,
    finish     date,
    price      number(10)    NOT NULL,
    CHECK ( price > 0 ),
    CHECK ( finish is null or finish > begin),
    CONSTRAINT cl_id_fk
        FOREIGN KEY (client_id)
            REFERENCES clients,
    CONSTRAINT emp_chief_fk
        FOREIGN KEY (manager_id)
            REFERENCES workers (workers_id)
);

CREATE TABLE "18207_MATEYUK".performers
(
    project_abbr char(20),
    worker_id    number(6),
    role         varchar2(50) NOT NULL,
    PRIMARY KEY (project_abbr, worker_id),
    FOREIGN KEY (project_abbr)
        REFERENCES projects (abbr),
    FOREIGN KEY (worker_id)
        REFERENCES workers (workers_id)
);

CREATE TABLE "18207_MATEYUK".subcontractors
(
    subcontractor_id number(6) PRIMARY KEY,
    company          varchar2(100) NOT NULL,
    address          varchar2(100) NOT NULL,
    manager          varchar2(100) NOT NULL,
    price            number(10)    NOT NULL,
    phone            varchar2(20),
    CHECK (price > 0)
);

CREATE TABLE "18207_MATEYUK".contract_stages
(
    contract_id   number(6),
    project_abbr  char(20),
    subcontractor number(6) NULL,
    PRIMARY KEY (contract_id, project_abbr),
    FOREIGN KEY (contract_id)
        REFERENCES contracts (contract_id),
    FOREIGN KEY (project_abbr)
        REFERENCES projects (abbr),
    FOREIGN KEY (subcontractor)
        REFERENCES subcontractors (subcontractor_id)
);

CREATE TABLE "18207_MATEYUK".instruments
(
    serial_id number(6) PRIMARY KEY,
    title     varchar2(50) NOT NULL,
    owner_id  number(6),
    FOREIGN KEY (owner_id)
        REFERENCES departments (department_id)
);

CREATE TABLE "18207_MATEYUK".accounting_instruments
(
    serial_id    number(6),
    project_abbr char(20),
    PRIMARY KEY (serial_id, project_abbr),
    FOREIGN KEY (project_abbr)
        REFERENCES projects (abbr)
);

BEGIN

    CREATE_AUTOINCREMENT('departments', 'department_id');
    CREATE_AUTOINCREMENT('positions', 'position_id');
    CREATE_AUTOINCREMENT('workers', 'workers_id');
    CREATE_AUTOINCREMENT('clients', 'client_id');
    CREATE_AUTOINCREMENT('contracts', 'contract_id');
    CREATE_AUTOINCREMENT('projects', 'project_id');
    CREATE_AUTOINCREMENT('subcontractors', 'subcontractor_id');
    CREATE_AUTOINCREMENT('instruments', 'serial_id');

END;
