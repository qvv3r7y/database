CREATE ROLE administrator_mateyuk;
CREATE ROLE client_mateyuk;
CREATE ROLE workers_mateyuk;
CREATE ROLE hr_mateyuk;

GRANT ALL ON "18207_MATEYUK".ACCOUNTING_INSTRUMENTS to administrator_mateyuk;
GRANT ALL ON "18207_MATEYUK".INSTRUMENTS to administrator_mateyuk;
GRANT ALL ON "18207_MATEYUK".CONTRACT_STAGES to administrator_mateyuk;
GRANT ALL ON "18207_MATEYUK".SUBCONTRACTORS to administrator_mateyuk;
GRANT ALL ON "18207_MATEYUK".PERFORMERS to administrator_mateyuk;
GRANT ALL ON "18207_MATEYUK".PROJECTS to administrator_mateyuk;
GRANT ALL ON "18207_MATEYUK".CONTRACTS to administrator_mateyuk;
GRANT ALL ON "18207_MATEYUK".CLIENTS to administrator_mateyuk;
GRANT ALL ON "18207_MATEYUK".WORKERS_ENGINEER to administrator_mateyuk;
GRANT ALL ON "18207_MATEYUK".WORKERS_MANAGER to administrator_mateyuk;
GRANT ALL ON "18207_MATEYUK".WORKERS to administrator_mateyuk;
GRANT ALL ON "18207_MATEYUK".POSITIONS to administrator_mateyuk;
GRANT ALL ON "18207_MATEYUK".DEPARTMENTS to administrator_mateyuk;

GRANT SELECT ON "18207_MATEYUK".CONTRACT_STAGES to client_mateyuk;
GRANT SELECT ON "18207_MATEYUK".SUBCONTRACTORS to client_mateyuk;
GRANT SELECT ON "18207_MATEYUK".PERFORMERS to client_mateyuk;
GRANT SELECT, INSERT ON "18207_MATEYUK".PROJECTS to client_mateyuk;
GRANT SELECT, INSERT ON "18207_MATEYUK".CONTRACTS to client_mateyuk;
GRANT SELECT, INSERT ON "18207_MATEYUK".CLIENTS to client_mateyuk;

GRANT ALL ON "18207_MATEYUK".ACCOUNTING_INSTRUMENTS to workers_mateyuk;
GRANT ALL ON "18207_MATEYUK".INSTRUMENTS to workers_mateyuk;
GRANT ALL ON "18207_MATEYUK".CONTRACT_STAGES to workers_mateyuk;
GRANT ALL ON "18207_MATEYUK".SUBCONTRACTORS to workers_mateyuk;
GRANT ALL ON "18207_MATEYUK".PERFORMERS to workers_mateyuk;
GRANT ALL ON "18207_MATEYUK".PROJECTS to workers_mateyuk;
GRANT SELECT ON "18207_MATEYUK".CONTRACTS to workers_mateyuk;
GRANT SELECT ON "18207_MATEYUK".CLIENTS to workers_mateyuk;
GRANT SELECT ON "18207_MATEYUK".POSITIONS to workers_mateyuk;
GRANT SELECT ON "18207_MATEYUK".DEPARTMENTS to workers_mateyuk;

GRANT ALL ON "18207_MATEYUK".SUBCONTRACTORS to hr_mateyuk;
GRANT ALL ON "18207_MATEYUK".PERFORMERS to hr_mateyuk;
GRANT ALL ON "18207_MATEYUK".WORKERS_ENGINEER to hr_mateyuk;
GRANT ALL ON "18207_MATEYUK".WORKERS_MANAGER to hr_mateyuk;
GRANT ALL ON "18207_MATEYUK".WORKERS to hr_mateyuk;
GRANT ALL ON "18207_MATEYUK".POSITIONS to hr_mateyuk;
GRANT ALL ON "18207_MATEYUK".DEPARTMENTS to hr_mateyuk;

CREATE USER client_m IDENTIFIED BY client_m;
GRANT CONNECT, RESOURCE TO client_m;
GRANT client_mateyuk TO client_m;

CREATE USER worker_m IDENTIFIED BY worker_m;
GRANT CONNECT, RESOURCE TO worker_m;
GRANT workers_mateyuk TO worker_m;

CREATE USER hr_m IDENTIFIED BY hr_m;
GRANT CONNECT, RESOURCE TO hr_m;
GRANT hr_mateyuk TO hr_m;

SELECT w.workers_id, w.first_name, w.last_name, d.department_name from "18207_MATEYUK".WORKERS w
                                                                           inner join "18207_MATEYUK".departments d
                                                                                      on w.workers_id = d.chief_id
order by workers_id;

SELECT * from "18207_MATEYUK".contracts c
where c.finish IS NULL or c.finish > sysdate
order by c.finish;

select i.serial_id, i.title, d.department_name from "18207_MATEYUK".INSTRUMENTS i
                                                        inner join "18207_MATEYUK".departments d
                                                                   on i.owner_id = d.department_id
where i.serial_id NOT IN
      (SELECT a.serial_id from ACCOUNTING_INSTRUMENTS a)
order by d.department_name;