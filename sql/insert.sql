INSERT INTO "18207_MATEYUK".DEPARTMENTS (department_id, department_name, chief_id) VALUES (1,'Отдел Аналитики', 6);
INSERT INTO "18207_MATEYUK".DEPARTMENTS (department_id, department_name, chief_id) VALUES (2,'Отдел Маркетинга', 7);
INSERT INTO "18207_MATEYUK".DEPARTMENTS (department_id, department_name, chief_id) VALUES (3,'Отдел Разработки', 1);
INSERT INTO "18207_MATEYUK".DEPARTMENTS (department_id, department_name, chief_id) VALUES (4,'Отдел продаж', 4);

insert into "18207_MATEYUK".POSITIONS (position_id, position_title) VALUES (1,'Дизайнер');
insert into "18207_MATEYUK".POSITIONS (position_id, position_title) VALUES (2,'Инженер');
insert into "18207_MATEYUK".POSITIONS (position_id, position_title) VALUES (3,'Конструктор');
insert into "18207_MATEYUK".POSITIONS (position_id, position_title) VALUES (4,'Ассистент');
insert into "18207_MATEYUK".POSITIONS (position_id, position_title) VALUES (5,'Маркетолог');
insert into "18207_MATEYUK".POSITIONS (position_id, position_title) VALUES (6,'Аналитик');

insert into "18207_MATEYUK".WORKERS (workers_id, first_name, last_name, phone_number, date_of_birth, position_id, salary, department_id, type) VALUES (1,'Илья','Матеюк',89002901010,'04.08.2000',2,80000,3, 'engineer');
insert into "18207_MATEYUK".WORKERS (workers_id, first_name, last_name, phone_number, date_of_birth, position_id, salary, department_id, type) VALUES (2,'Василий','Петров',89002909785,'04/04/2001',1,70000,3, 'engineer');
insert into "18207_MATEYUK".WORKERS (workers_id, first_name, last_name, phone_number, date_of_birth, position_id, salary, department_id, type) VALUES (3,'Петр','Сидоров',89002900530,'25/03/2003',3,60000,4, 'engineer');
insert into "18207_MATEYUK".WORKERS (workers_id, first_name, last_name, phone_number, date_of_birth, position_id, salary, department_id, type) VALUES (4,'Алиса','Синицын',89002142010,'24/05/2000',4,80000,4, 'engineer');
insert into "18207_MATEYUK".WORKERS (workers_id, first_name, last_name, phone_number, date_of_birth, position_id, salary, department_id, type) VALUES (5,'Данил','Павлов',89002793010,'14/10/2001',5,50000,2, 'manager');
insert into "18207_MATEYUK".WORKERS (workers_id, first_name, last_name, phone_number, date_of_birth, position_id, salary, department_id, type) VALUES (6,'Мария','Тищенко',89002902460,'24/11/2002',6,40000,1, 'manager');
insert into "18207_MATEYUK".WORKERS (workers_id, first_name, last_name, phone_number, date_of_birth, position_id, salary, department_id, type) VALUES (7,'Петр','Кот',89002901980,'08/12/2004',2,90000,4, 'manager');
insert into "18207_MATEYUK".WORKERS (workers_id, first_name, last_name, phone_number, date_of_birth, position_id, salary, department_id, type) VALUES (8,'Илья','Савин',89002761010,'12/09/2001',6,70000,1, 'manager');

insert into "18207_MATEYUK".WORKERS_ENGINEER (workers_id, academic_degree, experience, number_patents, specialization) VALUES (1, 'phD', 6, 10, 'proccesors');
insert into "18207_MATEYUK".WORKERS_ENGINEER (workers_id, academic_degree, experience, number_patents, specialization) VALUES (2, 'docent', 5, 8, 'phisics');
insert into "18207_MATEYUK".WORKERS_ENGINEER (workers_id, academic_degree, experience, number_patents, specialization) VALUES (3, 'science worker',3, 12, 'math');
insert into "18207_MATEYUK".WORKERS_ENGINEER (workers_id, academic_degree, experience, number_patents, specialization) VALUES (4, 'professor', 1, 0, 'IT');

insert into "18207_MATEYUK".WORKERS_MANAGER (workers_id, efficiency, mentoring_experience, access_level) VALUES (5, 8.77, 10, 3);
insert into "18207_MATEYUK".WORKERS_MANAGER (workers_id, efficiency, mentoring_experience, access_level) VALUES (6, 6.89, 4, 2);
insert into "18207_MATEYUK".WORKERS_MANAGER (workers_id, efficiency, mentoring_experience, access_level) VALUES (7, 4.36, 1, 1);
insert into "18207_MATEYUK".WORKERS_MANAGER (workers_id, efficiency, mentoring_experience, access_level) VALUES (8, 9.31, 15, 5);

insert into "18207_MATEYUK".CLIENTS (company, address, person, phone) VALUES ('ООО РОГА И КОПЫТА','г.Москва, ул.Вязов, 50','Иванов Иван Петрович',89002003010);
insert into "18207_MATEYUK".CLIENTS (company, address, person, phone) VALUES ('ОАО ВЕКТОР','г.Новосибирск, ул.Строителей, 32','Петров Петр Иванович',89961053515);

INSERT INTO "18207_MATEYUK".CONTRACTS (bank_details, conclusions, begin, finish, client_id, manager_id) VALUES (9120939024934,'20/05/2020','01/09/2020','03/04/2021',1,4);
INSERT INTO "18207_MATEYUK".CONTRACTS (bank_details, conclusions, begin, finish, client_id, manager_id) VALUES (8265020574832,'10/01/2021','01/02/2021',NULL,2,1);

INSERT INTO "18207_MATEYUK".PROJECTS (title, abbr, client_id, manager_id, begin, finish, price) VALUES ('Разработка БД', 'DEV_DB',1,1,'01/09/2020','03/04/2021', 500000);
INSERT INTO "18207_MATEYUK".PROJECTS (title, abbr, client_id, manager_id, begin, finish, price) VALUES ('Разработка документооборота', 'DEV_DOC',2,5,'01/02/2021','03/04/2021', 100000);
INSERT INTO "18207_MATEYUK".PROJECTS (title, abbr, client_id, manager_id, begin, finish, price) VALUES ('Поддержка документооборота', 'SUP_DOC',2,5,'03/04/2021','03/05/2021', 500000);

INSERT INTO "18207_MATEYUK".CONTRACT_STAGES (contract_id, project_abbr, subcontractor) VALUES (1,'DEV_DB', NULL);
INSERT INTO "18207_MATEYUK".CONTRACT_STAGES (contract_id, project_abbr, subcontractor) VALUES (2,'DEV_DOC', NULL);
INSERT INTO "18207_MATEYUK".CONTRACT_STAGES (contract_id, project_abbr, subcontractor) VALUES (2,'SUP_DOC', NULL);

INSERT INTO "18207_MATEYUK".PERFORMERS (project_abbr, worker_id, role) VALUES ('DEV_DB',1, 'Разработка');
INSERT INTO "18207_MATEYUK".PERFORMERS (project_abbr, worker_id, role) VALUES ('DEV_DB',2, 'Тестирование');
INSERT INTO "18207_MATEYUK".PERFORMERS (project_abbr, worker_id, role) VALUES ('DEV_DB',5, 'Деплой');
INSERT INTO "18207_MATEYUK".PERFORMERS (project_abbr, worker_id, role) VALUES ('DEV_DOC',3,'Разработка');
INSERT INTO "18207_MATEYUK".PERFORMERS (project_abbr, worker_id, role) VALUES ('DEV_DOC',1, 'Разработка');
INSERT INTO "18207_MATEYUK".PERFORMERS (project_abbr, worker_id, role) VALUES ('SUP_DOC',4, 'Поддержка');

INSERT INTO "18207_MATEYUK".INSTRUMENTS (title, owner_id) VALUES ('Ubuntu 20.0 2xi7 Server', 3);
INSERT INTO "18207_MATEYUK".INSTRUMENTS (title, owner_id) VALUES ('Windows 10 4xi5 Server', 2);

INSERT INTO "18207_MATEYUK".ACCOUNTING_INSTRUMENTS (serial_id, project_abbr) VALUES (1,'SUP_DOC');

INSERT INTO "18207_MATEYUK".SUBCONTRACTORS (company, address, manager, price, phone) VALUES ('WEIGHT AND CO', 'Москва, ул.Длинная, 9', 'Могучий Алексей Витальевич', 35000, '89995550715');
