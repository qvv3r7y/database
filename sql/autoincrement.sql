CREATE OR REPLACE PROCEDURE CREATE_AUTOINCREMENT
	(tableName IN VARCHAR2, columnName IN VARCHAR2)
IS
	rowsCount INTEGER;
	BEGIN
	SELECT count(*)  INTO rowsCount	FROM user_sequences 
	 WHERE sequence_name = trim(upper(tableName || 'Sequence'));
	IF rowsCount > 0
	THEN
		DBMS_OUTPUT.PUT_LINE('Sequence for table ' || tableName || ' exist, will drop');
		EXECUTE IMMEDIATE 'DROP SEQUENCE ' || tableName || 'Sequence';
	END IF;
	
	EXECUTE IMMEDIATE 'CREATE SEQUENCE ' || tableName || 'Sequence START WITH 1 INCREMENT BY 1';
	DBMS_OUTPUT.PUT_LINE('Sequence for table ' || tableName || ' created'); 
	
	EXECUTE IMMEDIATE 'CREATE OR REPLACE TRIGGER ' || tableName || 'Increment
BEFORE INSERT ON ' || tableName ||
' FOR EACH ROW
BEGIN
	SELECT ' || tableName || 'Sequence.nextval into :new.' || columnName || ' from dual;
END; ';
	
END;