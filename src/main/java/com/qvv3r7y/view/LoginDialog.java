package com.qvv3r7y.view;

import com.qvv3r7y.Controller;
import com.qvv3r7y.view.utils.BoxLayoutUtils;
import com.qvv3r7y.view.utils.GUITools;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.sql.SQLException;

public class LoginDialog extends JDialog {
    private static final long serialVersionUID = 1L;

    public JTextField tfLogin;
    public JPasswordField tfPassword;
    public JButton btnOk, btnCancel, btnDefault;

    public LoginDialog(JFrame parent) {
        super(parent, "Вход в систему");
        // При выходе из диалогового окна работа заканчивается
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent we) {
                dispose();
                System.exit(0);
            }
        });
        // добавляем расположение в центр окна
        getContentPane().add(createGUI());
        // задаем предпочтительный размер
        pack();
        setLocationRelativeTo(null);
        // выводим окно на экран
        setVisible(true);
    }

    // этот метод будет возвращать панель с созданным расположением
    private JPanel createGUI() {
        // Создание панели для размещение компонентов
        JPanel panel = BoxLayoutUtils.createVerticalPanel();
        // Определение отступов от границ панели. Для этого используем пустую рамку
        panel.setBorder(BorderFactory.createEmptyBorder(12, 12, 12, 12));
        // Создание панели для размещения метки и текстового поля логина
        JPanel name = BoxLayoutUtils.createHorizontalPanel();
        JLabel nameLabel = new JLabel("Login:");
        name.add(nameLabel);
        name.add(Box.createHorizontalStrut(12));
        tfLogin = new JTextField(15);
        name.add(tfLogin);
        // Создание панели для размещения метки и текстового поля пароля
        JPanel password = BoxLayoutUtils.createHorizontalPanel();
        JLabel passwrdLabel = new JLabel("Password:");
        password.add(passwrdLabel);
        password.add(Box.createHorizontalStrut(12));
        tfPassword = new JPasswordField(15);
        password.add(tfPassword);
        // Создание панели для размещения кнопок управления
        JPanel flow = new JPanel(new FlowLayout(FlowLayout.RIGHT, 0, 0));
        JPanel grid = new JPanel(new GridLayout(1, 2, 5, 0));
        btnOk = new JButton("Sign in");
        btnCancel = new JButton("Cancel");
        btnDefault = new JButton("Admin");
        grid.add(btnOk);
        grid.add(btnCancel);
        grid.add(btnDefault);
        flow.add(grid);
        // Выравнивание вложенных панелей по горизонтали
        BoxLayoutUtils.setGroupAlignmentX(new JComponent[]{name, password, panel, flow},
                Component.LEFT_ALIGNMENT);
        // Выравнивание вложенных панелей по вертикали
        BoxLayoutUtils.setGroupAlignmentY(new JComponent[]{tfLogin, tfPassword, nameLabel, passwrdLabel},
                Component.CENTER_ALIGNMENT);
        // Определение размеров надписей к текстовым полям
        GUITools.makeSameSize(new JComponent[]{nameLabel, passwrdLabel});
        // Определение стандартного вида для кнопок
        GUITools.createRecommendedMargin(new JButton[]{btnDefault, btnOk, btnCancel});
        // Устранение "бесконечной" высоты текстовых полей
        GUITools.fixTextFieldSize(tfLogin);
        GUITools.fixTextFieldSize(tfPassword);
        addButtonAction();
        // Сборка интерфейса
        panel.add(name);
        panel.add(Box.createVerticalStrut(12));
        panel.add(password);
        panel.add(Box.createVerticalStrut(17));
        panel.add(flow);
        // готово
        return panel;
    }

    public void addButtonAction() {
        btnCancel.addActionListener(event -> {
            dispose();
            System.exit(0);
        });
        btnDefault.addActionListener(event -> {
            tfLogin.setText(Controller.DEFAULT_USERNAME);
            tfPassword.setText(Controller.DEFAULT_PASSWORD);
        });
        btnOk.addActionListener(event -> {
            Controller controller = new Controller();
            try {
                controller.login(Controller.DEFAULT_DATABASE, tfLogin.getText(), new String(tfPassword.getPassword()));
            }catch (SQLException e){
                JOptionPane.showMessageDialog(new JFrame(),"Wrong login or password. Try again...");
                return;
            }
            dispose();
            MainMenu menu = new MainMenu(controller);
        });
    }
}