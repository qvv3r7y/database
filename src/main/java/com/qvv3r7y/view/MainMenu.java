package com.qvv3r7y.view;

import com.qvv3r7y.Controller;
import com.qvv3r7y.repository.TableOperations;
import com.qvv3r7y.repository.UserCreator;
import com.qvv3r7y.repository.requests.FreeInstruments;
import com.qvv3r7y.repository.requests.Managers;
import com.qvv3r7y.repository.requests.OpenContracts;
import com.qvv3r7y.view.table_model.entity.*;
import com.qvv3r7y.view.table_model.requests.FreeInstrumentsTableModel;
import com.qvv3r7y.view.table_model.requests.ManagersTableModel;
import com.qvv3r7y.view.table_model.requests.OpenContractsTableModel;
import com.qvv3r7y.view.utils.BoxLayoutUtils;
import com.qvv3r7y.view.utils.GUITools;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import java.awt.*;
import java.sql.SQLException;
import java.util.concurrent.atomic.AtomicReference;

public class MainMenu extends JFrame {
    private static final long serialVersionUID = 1L;
    private final Controller controller;

    public MainMenu(Controller controller) {
        super("Menu");
        this.controller = controller;
        JPanel menu = new JPanel();
        GridLayout layout = new GridLayout(6, 3, 15, 15);
        menu.setLayout(layout);
        addButtons(menu);
        setContentPane(menu);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        setSize(800, 350);
        setVisible(true);
    }

    public void createRequestPanel() {
        JFrame frame = new JFrame("Requests");
        JPanel requestsPanel = new JPanel();
        GridLayout layout = new GridLayout(3, 5, 15, 15);
        requestsPanel.setLayout(layout);

        JButton button1 = new JButton("Показать свободные инструменты");
        JButton button2 = new JButton("Показать всех менеджеров");
        JButton button3 = new JButton("Показать свободные контракты в работе");
        button1.addActionListener(e -> {
            FreeInstrumentsTableModel model = new FreeInstrumentsTableModel();
            model.addData(new FreeInstruments("FreeInstruments",controller.getConnection()).executeRequest());
            createRequestTable(model);
        });
        button2.addActionListener(e -> {
            ManagersTableModel model = new ManagersTableModel();
            model.addData(new Managers("Managers",controller.getConnection()).executeRequest());
            createRequestTable(model);
        });
        button3.addActionListener(e -> {
            OpenContractsTableModel model = new OpenContractsTableModel();
            model.addData(new OpenContracts("OpenContracts",controller.getConnection()).executeRequest());
            createRequestTable(model);
        });

        requestsPanel.add(button1);
        requestsPanel.add(button2);
        requestsPanel.add(button3);

        frame.setContentPane(requestsPanel);
        frame.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.setSize(400, 250);
        frame.setVisible(true);
    }

    public void createTable(AbstractTableModel model, TableOperations dbTable) {
        JFrame frame = new JFrame("Result");
        frame.setSize(new Dimension(600, 400));
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.setLayout(new GridBagLayout());

        JTable table = new JTable(model);
        JScrollPane tableScrollPage = new JScrollPane(table);
        tableScrollPage.setPreferredSize(new Dimension(750, 400));


        JButton addBtn = new JButton("Add");
        addBtn.setBackground(Color.GREEN);
        JButton delBtn = new JButton("Delete");
        delBtn.setBackground(Color.RED);

        addBtn.addActionListener(event -> {
            addNewRow(model, dbTable, frame);
        });
        delBtn.addActionListener(event -> {
            deleteRow(dbTable, frame);
        });

        frame.add(tableScrollPage, new GridBagConstraints(0, 0, 2, 1, 1, 1, GridBagConstraints.NORTH, GridBagConstraints.BOTH, new Insets(1, 1, 1, 1), 0, 0));
        frame.add(addBtn, new GridBagConstraints(0, 1, 1, 1, 1, 1, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(1, 1, 1, 1), 0, 0));
        frame.add(delBtn, new GridBagConstraints(1, 1, 1, 1, 1, 1, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(1, 1, 1, 1), 0, 0));
        frame.setVisible(true);
        frame.pack();
    }

    public void createRequestTable(AbstractTableModel model) {
        JFrame frame = new JFrame("Result");
        frame.setSize(new Dimension(600, 400));
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.setLayout(new GridBagLayout());

        JTable table = new JTable(model);
        JScrollPane tableScrollPage = new JScrollPane(table);
        tableScrollPage.setPreferredSize(new Dimension(750, 400));


        frame.add(tableScrollPage, new GridBagConstraints(0, 0, 2, 1, 1, 1, GridBagConstraints.NORTH, GridBagConstraints.BOTH, new Insets(1, 1, 1, 1), 0, 0));
        frame.setVisible(true);
        frame.pack();
    }

    public void deleteRow(TableOperations dbTable, JFrame table) {
        JFrame frame = new JFrame("Delete row");
        frame.setSize(new Dimension(600, 400));
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.setLayout(new GridBagLayout());
        JPanel panel = BoxLayoutUtils.createVerticalPanel();
        // Определение отступов от границ панели. Для этого используем пустую рамку
        panel.setBorder(BorderFactory.createEmptyBorder(12, 12, 12, 12));
        // Создание панели для размещения метки и текстового поля логина
        JPanel name = BoxLayoutUtils.createHorizontalPanel();
        JLabel nameLabel = new JLabel("ID \\ Primary key:");
        name.add(nameLabel);
        name.add(Box.createHorizontalStrut(12));
        JTextField del = new JTextField(10);
        name.add(del);
        // Создание панели для размещения метки и текстового поля пароля
        // Создание панели для размещения кнопок управления
        JPanel flow = new JPanel(new FlowLayout(FlowLayout.RIGHT, 0, 0));
        JPanel grid = new JPanel(new GridLayout(1, 2, 5, 0));
        JButton btnOk = new JButton("Ok");
        btnOk.setBackground(Color.RED);
        btnOk.addActionListener(event -> {
            String key = del.getText();
            try {
                dbTable.deleteRow(key);
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
            table.dispose();
            frame.dispose();
        });
        grid.add(btnOk);
        flow.add(grid);
        // Выравнивание вложенных панелей по горизонтали
        // Сборка интерфейса
        panel.add(name);
        panel.add(Box.createVerticalStrut(12));
        panel.add(Box.createVerticalStrut(17));
        panel.add(flow);
        frame.getContentPane().add(panel);
        frame.pack();
        frame.setVisible(true);
    }

    public void addNewRow(AbstractTableModel model, TableOperations dbTable, JFrame table) {
        JFrame frame = new JFrame("Add new row");
        frame.setSize(new Dimension(600, 400));
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.setLayout(new GridBagLayout());

        JPanel panel = BoxLayoutUtils.createVerticalPanel();
        // Определение отступов от границ панели. Для этого используем пустую рамку
        panel.setBorder(BorderFactory.createEmptyBorder(12, 12, 12, 12));
        // Создание панели для размещения метки и текстового поля логина

        JPanel[] panels = new JPanel[model.getColumnCount() + 2];
        JLabel[] labels = new JLabel[model.getColumnCount()];
        JTextField[] fields = new JTextField[model.getColumnCount()];
        for (int i = 0; i < model.getColumnCount(); i++) {
            JPanel p = BoxLayoutUtils.createHorizontalPanel();
            JLabel l = new JLabel(model.getColumnName(i));
            p.add(l);
            p.add(Box.createHorizontalStrut(12));
            JTextField f = new JTextField(25);
            GUITools.fixTextFieldSize(f);
            p.add(f);

            panels[i] = p;
            labels[i] = l;
            fields[i] = f;

            panel.add(p);
        }

        JPanel flow = new JPanel(new FlowLayout(FlowLayout.RIGHT, 0, 0));
        JPanel grid = new JPanel(new GridLayout(1, 2, 5, 0));

        JButton btnOk = new JButton("Ok");
        btnOk.addActionListener(event -> {
            String[] row = new String[model.getColumnCount()];
            for (int i = 0; i < fields.length; i++) {
                row[i] = fields[i].getText();
            }
            if (model.getColumnName(0).equals("id")) {
                System.arraycopy(row, 1, row, 0, fields.length - 1);
            }
            try {
                dbTable.insertRow(row);
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
            frame.dispose();
            table.dispose();
        });
        grid.add(btnOk);
        flow.add(grid);
        // Выравнивание вложенных панелей по горизонтали
        panels[model.getColumnCount()] = panel;
        panels[model.getColumnCount() + 1] = flow;
        BoxLayoutUtils.setGroupAlignmentX(panels,
                Component.LEFT_ALIGNMENT);
        // Выравнивание вложенных панелей по вертикали
        BoxLayoutUtils.setGroupAlignmentY(labels,
                Component.CENTER_ALIGNMENT);
        // Определение размеров надписей к текстовым полям
        GUITools.makeSameSize(labels);
        // Сборка интерфейса
        panel.add(Box.createVerticalStrut(12));
        panel.add(Box.createVerticalStrut(17));
        panel.add(flow);

        frame.getContentPane().add(panel);
        frame.pack();
        frame.setVisible(true);
    }

    public void addUserForm(UserCreator userCreator) {
        JFrame frame = new JFrame("Add new user");
        frame.setSize(new Dimension(600, 400));
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.setLayout(new GridBagLayout());

        AtomicReference<UserCreator.Roles> pickRole = new AtomicReference<>();
        JCheckBox adminRole = new JCheckBox("Admin");
        JCheckBox hrRole = new JCheckBox("HR");
        JCheckBox clientRole = new JCheckBox("Client");
        JCheckBox workerRole = new JCheckBox("Worker");
        JTextField tfLogin;
        JPasswordField tfPassword;
        JPanel panel = BoxLayoutUtils.createVerticalPanel();
        // Определение отступов от границ панели. Для этого используем пустую рамку
        panel.setBorder(BorderFactory.createEmptyBorder(12, 12, 12, 12));
        // Создание панели для размещения метки и текстового поля логина
        JPanel name = BoxLayoutUtils.createHorizontalPanel();
        JLabel nameLabel = new JLabel("Login:");
        name.add(nameLabel);
        name.add(Box.createHorizontalStrut(12));
        tfLogin = new JTextField(15);
        name.add(tfLogin);
        // Создание панели для размещения метки и текстового поля пароля
        JPanel password = BoxLayoutUtils.createHorizontalPanel();
        JLabel passwrdLabel = new JLabel("Password:");
        password.add(passwrdLabel);
        password.add(Box.createHorizontalStrut(12));
        tfPassword = new JPasswordField(15);
        password.add(tfPassword);

        JPanel flow = new JPanel(new FlowLayout(FlowLayout.RIGHT, 0, 0));
        JPanel grid = new JPanel(new GridLayout(1, 2, 5, 0));

        hrRole.addItemListener(ev -> pickRole.set(UserCreator.Roles.HR));
        adminRole.addItemListener(ev -> pickRole.set(UserCreator.Roles.ADMIN));
        clientRole.addItemListener(ev -> pickRole.set(UserCreator.Roles.CLIENT));
        workerRole.addItemListener(ev -> pickRole.set(UserCreator.Roles.WORKER));

        JButton btnOk = new JButton("Ok");
        btnOk.addActionListener(event -> {
            try {
                userCreator.createNewUser(tfLogin.getText(), new String(tfPassword.getPassword()), pickRole.get());
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
            frame.dispose();
        });
        grid.add(btnOk);
        flow.add(grid);
        BoxLayoutUtils.setGroupAlignmentX(new JComponent[]{name, password, panel, flow},
                Component.LEFT_ALIGNMENT);
        BoxLayoutUtils.setGroupAlignmentY(new JComponent[]{tfLogin, tfPassword, nameLabel, passwrdLabel},
                Component.CENTER_ALIGNMENT);
        GUITools.makeSameSize(new JComponent[]{nameLabel, passwrdLabel});
        GUITools.createRecommendedMargin(new JButton[]{btnOk});
        GUITools.fixTextFieldSize(tfLogin);
        GUITools.fixTextFieldSize(tfPassword);
        panel.add(name);
        panel.add(Box.createVerticalStrut(12));
        panel.add(password);
        panel.add(Box.createVerticalStrut(17));
        panel.add(adminRole);
        panel.add(hrRole);
        panel.add(clientRole);
        panel.add(workerRole);
        panel.add(flow);

        frame.getContentPane().add(panel);
        frame.pack();
        frame.setVisible(true);
    }

    public void addButtons(JPanel menu) {
        JButton button3 = new JButton("Удалить все таблицы");
        button3.addActionListener(e -> controller.deleteTables());
        button3.setBackground(Color.ORANGE);
        JButton button4 = new JButton("Создать таблицы");
        button4.addActionListener(e -> {
            try {
                controller.createTablesAndForeignKeys();
            } catch (SQLException er) {
                er.printStackTrace();
            }
        });
        button4.setBackground(Color.ORANGE);
        JButton button5 = new JButton("Вставить тестовые данные");
        button5.addActionListener(e -> controller.fillTablesDefaultData());
        button5.setBackground(Color.ORANGE);
        JButton button6 = new JButton("Персонал");
        button6.addActionListener(e -> {
            WorkersTableModel model = new WorkersTableModel();
            model.addData(controller.getWorkers());
            createTable(model, controller.getWorkers());
        });
        JButton button18 = new JButton("Инженеры");
        button18.addActionListener(e -> {
            WorkersEngineerTableModel model = new WorkersEngineerTableModel();
            model.addData(controller.getWorkersEngineer());
            createTable(model, controller.getWorkersEngineer());
        });
        JButton button19 = new JButton("Менеджеры");
        button19.addActionListener(e -> {
            WorkersManagerTableModel model = new WorkersManagerTableModel();
            model.addData(controller.getWorkersManager());
            createTable(model, controller.getWorkersManager());
        });
        JButton button7 = new JButton("Должности");
        button7.addActionListener(e -> {
            PositionsTableModel model = new PositionsTableModel();
            model.addData(controller.getPositions());
            createTable(model, controller.getPositions());
        });
        JButton button8 = new JButton("Отделы");
        button8.addActionListener(e -> {
            DepartmentsTableModel model = new DepartmentsTableModel();
            model.addData(controller.getDepartments());
            createTable(model, controller.getDepartments());
        });
        JButton button9 = new JButton("Заказчики");
        button9.addActionListener(e -> {
            ClientsTableModel model = new ClientsTableModel();
            model.addData(controller.getClients());
            createTable(model, controller.getClients());
        });
        JButton button10 = new JButton("Контракты");
        button10.addActionListener(e -> {
            ContractsTableModel model = new ContractsTableModel();
            model.addData(controller.getContracts());
            createTable(model, controller.getContracts());
        });
        JButton button11 = new JButton("Проекты");
        button11.addActionListener(e -> {
            ProjectsTableModel model = new ProjectsTableModel();
            model.addData(controller.getProjects());
            createTable(model, controller.getProjects());
        });
        JButton button12 = new JButton("Исполнители");
        button12.addActionListener(e -> {
            PerformersTableModel model = new PerformersTableModel();
            model.addData(controller.getPerformers());
            createTable(model, controller.getPerformers());
        });
        JButton button13 = new JButton("Инструменты");
        button13.addActionListener(e -> {
            InstrumentsTableModel model = new InstrumentsTableModel();
            model.addData(controller.getInstruments());
            createTable(model, controller.getInstruments());
        });
        JButton button14 = new JButton("Учет инструментов");
        button14.addActionListener(e -> {
            AccountingInstrumentsTableModel model = new AccountingInstrumentsTableModel();
            model.addData(controller.getAccountingInstruments());
            createTable(model, controller.getAccountingInstruments());
        });
        JButton button15 = new JButton("Субподрядчики");
        button15.addActionListener(e -> {
            SubcontractorsTableModel model = new SubcontractorsTableModel();
            model.addData(controller.getSubcontractors());
            createTable(model, controller.getSubcontractors());
        });
        JButton button16 = new JButton("Этапы контрактов");
        button16.addActionListener(e -> {
            ContractStagesTableModel model = new ContractStagesTableModel();
            model.addData(controller.getContractStages());
            createTable(model, controller.getContractStages());
        });
        JButton button17 = new JButton("Запросы");
        button17.setBackground(Color.CYAN);
        button17.addActionListener(e -> {
            createRequestPanel();
        });
        JButton button20 = new JButton("Добавить пользователя");
        button20.setBackground(Color.orange);
        button20.addActionListener(e -> {
            addUserForm(controller.getUserCreator());
        });

        // Создание панели содержимого с размещением кнопок
        menu.add(button3);
        menu.add(button4);
        menu.add(button5);
        menu.add(button6);
        menu.add(button18);
        menu.add(button19);
        menu.add(button7);
        menu.add(button8);
        menu.add(button9);
        menu.add(button10);
        menu.add(button11);
        menu.add(button12);
        menu.add(button13);
        menu.add(button14);
        menu.add(button15);
        menu.add(button16);
        menu.add(button17);
        menu.add(button20);
    }

}