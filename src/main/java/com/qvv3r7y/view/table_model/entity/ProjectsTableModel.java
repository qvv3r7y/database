package com.qvv3r7y.view.table_model.entity;

import com.qvv3r7y.repository.TableOperations;

import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;

public class ProjectsTableModel extends AbstractTableModel {

    private int columnCount = 8;
    private ArrayList<String[]> dataArrayList;

    public ProjectsTableModel() {
        dataArrayList = new ArrayList<>();
    }

    @Override
    public int getRowCount() {
        return dataArrayList.size();
    }

    @Override
    public int getColumnCount() {
        return columnCount;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        String[] rows = dataArrayList.get(rowIndex);
        return rows[columnIndex];
    }

    @Override
    public String getColumnName(int columnIndex) {
        switch (columnIndex) {
            case 0:
                return "PROJECT_ID";
            case 1:
                return "TITLE";
            case 2:
                return "ABBR";
            case 3:
                return "CLIENT_ID";
            case 4:
                return "MANAGER_ID";
            case 5:
                return "BEGIN";
            case 6:
                return "FINISH";
            case 7:
                return "PRICE";
            default:
                return "";
        }
    }

    public void addData(String[] row) {
        String[] rowTable = new String[getColumnCount()];
        rowTable = row;
        dataArrayList.add(rowTable);
    }
    public void addData(TableOperations table){
        ArrayList<String[]> data = table.getAllData();
        for (String[] row : data){
            addData(row);
        }
    }
}

