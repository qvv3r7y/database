package com.qvv3r7y.view.table_model.entity;

import com.qvv3r7y.repository.TableOperations;

import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;

public class WorkersManagerTableModel extends AbstractTableModel {

    private int columnCount = 4;
    private ArrayList<String[]> dataArrayList;

    public WorkersManagerTableModel() {
        dataArrayList = new ArrayList<>();
    }

    @Override
    public int getRowCount() {
        return dataArrayList.size();
    }

    @Override
    public int getColumnCount() {
        return columnCount;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        String[] rows = dataArrayList.get(rowIndex);
        return rows[columnIndex];
    }

    @Override
    public String getColumnName(int columnIndex) {
        switch (columnIndex) {
            case 0:
                return "id";
            case 1:
                return "efficiency";
            case 2:
                return "mentoring experience";
            case 3:
                return "access level";
            default:
                return "";
        }
    }

    public void addData(String[] row) {
        String[] rowTable = new String[getColumnCount()];
        rowTable = row;
        dataArrayList.add(rowTable);
    }
    public void addData(TableOperations table){
        ArrayList<String[]> data = table.getAllData();
        for (String[] row : data){
            addData(row);
        }
    }
}
