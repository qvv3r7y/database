package com.qvv3r7y.view.table_model.requests;

import com.qvv3r7y.repository.TableOperations;

import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;

public class OpenContractsTableModel extends AbstractTableModel {

    private int columnCount = 7;
    private ArrayList<String[]> dataArrayList;

    public OpenContractsTableModel() {
        dataArrayList = new ArrayList<>();
    }

    @Override
    public int getRowCount() {
        return dataArrayList.size();
    }

    @Override
    public int getColumnCount() {
        return columnCount;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        String[] rows = dataArrayList.get(rowIndex);
        return rows[columnIndex];
    }

    @Override
    public String getColumnName(int columnIndex) {
        switch (columnIndex) {
            case 0:
                return "CONTRACT_ID";
            case 1:
                return "BANK_DETAILS";
            case 2:
                return "CONCLUSIONS";
            case 3:
                return "BEGIN";
            case 4:
                return "FINISH";
            case 5:
                return "CLIENT_ID";
            case 6:
                return "MANAGER ID";
            default:
                return "";
        }
    }

    public void addData(String[] row) {
        String[] rowTable = new String[getColumnCount()];
        rowTable = row;
        dataArrayList.add(rowTable);
    }
    public void addData(TableOperations table){
        ArrayList<String[]> data = table.getAllData();
        for (String[] row : data){
            addData(row);
        }
    }
    public void addData(ArrayList<String[]> data){
        for (String[] row : data){
            addData(row);
        }
    }
}
