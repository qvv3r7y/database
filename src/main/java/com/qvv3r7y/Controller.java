package com.qvv3r7y;

import com.qvv3r7y.repository.UserCreator;
import com.qvv3r7y.repository.entity.*;

import java.sql.*;

public class Controller {
    public static final String JDBC_ORACLE_DRIVER = "oracle.jdbc.driver.OracleDriver";
    public static final String DEFAULT_USERNAME = "18207_Mateyuk";
    public static final String DEFAULT_PASSWORD = "realman";
    public static final String DEFAULT_DATABASE = "jdbc:oracle:thin:@84.237.50.81:1521:XE";

    private String user;
    private String password;
    private String databaseUrl;

    private Connection connection;
    private UserCreator userCreator;

    private Departments departments;
    private Positions positions;
    private Workers workers;
    private WorkersEngineer workersEngineer;
    private WorkersManager workersManager;
    private Instruments instruments;
    private Performers performers;
    private Projects projects;
    private Contracts contracts;
    private Clients clients;
    private Subcontractors subcontractors;
    private AccountingInstruments accountingInstruments;
    private ContractStages contractStages;

    public Controller() {
        try {
            Class.forName(JDBC_ORACLE_DRIVER);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void login(String databaseUrl, String user, String password) throws SQLException {
        this.user = user;
        this.password = password;
        this.databaseUrl = databaseUrl;

        this.userCreator = new UserCreator(getConnection());

        departments = new Departments(getConnection());
        positions = new Positions(getConnection());
        workers = new Workers(getConnection());
        workersEngineer = new WorkersEngineer(getConnection());
        workersManager = new WorkersManager(getConnection());
        clients = new Clients(getConnection());
        contracts = new Contracts(getConnection());
        projects = new Projects(getConnection());
        performers = new Performers(getConnection());
        subcontractors = new Subcontractors(getConnection());
        contractStages = new ContractStages(getConnection());
        instruments = new Instruments(getConnection());
        accountingInstruments = new AccountingInstruments(getConnection());
    }

    public Connection getConnection()  {
        if (connection != null) return connection;
        try {
            connection = DriverManager.getConnection(databaseUrl, user, password);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return connection;
    }


    public void deleteTables() {
        accountingInstruments.deleteTable();
        instruments.deleteTable();
        contractStages.deleteTable();
        subcontractors.deleteTable();
        performers.deleteTable();
        projects.deleteTable();
        contracts.deleteTable();
        clients.deleteTable();
        workersManager.deleteTable();
        workersEngineer.deleteTable();
        workers.deleteTable();
        positions.deleteTable();
        departments.deleteTable();
    }

    public void createTablesAndForeignKeys() throws SQLException {
        departments.createTable();
        positions.createTable();
        workers.createTable();
        workersEngineer.createTable();
        workersManager.createTable();
        clients.createTable();
        contracts.createTable();
        projects.createTable();
        performers.createTable();
        subcontractors.createTable();
        contractStages.createTable();
        instruments.createTable();
        accountingInstruments.createTable();
    }

    public void fillTablesDefaultData() {
        departments.insertDefaultData();
        positions.insertDefaultData();
        workers.insertDefaultData();
        workersManager.insertDefaultData();
        workersEngineer.insertDefaultData();
        clients.insertDefaultData();
        contracts.insertDefaultData();
        projects.insertDefaultData();
        performers.insertDefaultData();
        subcontractors.insertDefaultData();
        contractStages.insertDefaultData();
        instruments.insertDefaultData();
        accountingInstruments.insertDefaultData();
    }

    public Departments getDepartments() {
        return departments;
    }

    public Positions getPositions() {
        return positions;
    }

    public Workers getWorkers() {
        return workers;
    }

    public Instruments getInstruments() {
        return instruments;
    }

    public Performers getPerformers() {
        return performers;
    }

    public Projects getProjects() {
        return projects;
    }

    public Contracts getContracts() {
        return contracts;
    }

    public Clients getClients() {
        return clients;
    }

    public UserCreator getUserCreator() {
        return userCreator;
    }

    public WorkersEngineer getWorkersEngineer() {
        return workersEngineer;
    }

    public WorkersManager getWorkersManager() {
        return workersManager;
    }

    public Subcontractors getSubcontractors() {
        return subcontractors;
    }

    public AccountingInstruments getAccountingInstruments() {
        return accountingInstruments;
    }

    public ContractStages getContractStages() {
        return contractStages;
    }
}
