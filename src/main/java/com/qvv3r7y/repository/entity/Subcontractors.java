package com.qvv3r7y.repository.entity;

import com.qvv3r7y.repository.BaseTable;
import com.qvv3r7y.repository.TableOperations;

import java.sql.*;
import java.util.ArrayList;
import java.util.Locale;

public class Subcontractors extends BaseTable implements TableOperations {
    public Subcontractors(Connection connection) throws SQLException {
        super("subcontractors", connection);
    }

    @Override
    public void createTable() throws SQLException {
        super.executeSqlStatement("CREATE TABLE \"18207_MATEYUK\".subcontractors\n" +
                "(\n" +
                "    subcontractor_id number(6) PRIMARY KEY,\n" +
                "    company          varchar2(100) NOT NULL,\n" +
                "    address          varchar2(100) NOT NULL,\n" +
                "    manager          varchar2(100) NOT NULL,\n" +
                "    price            number(10)    NOT NULL,\n" +
                "    phone            varchar2(20),\n" +
                "    CHECK (price > 0)\n" +
                ")", "Create table " + tableName);
        super.executeSqlStatement("begin CREATE_AUTOINCREMENT('subcontractors', 'subcontractor_id'); end;");
    }

    @Override
    public void deleteTable() {
        super.executeSqlStatement("drop table \"18207_MATEYUK\".subcontractors CASCADE CONSTRAINTS");
        super.executeSqlStatement("drop sequence \"18207_MATEYUK\"." + tableName.toUpperCase(Locale.ROOT) + "SEQUENCE");
    }

    @Override
    public void deleteAllData() {
        super.executeSqlStatement("delete from \"18207_MATEYUK\".SUBCONTRACTORS");
    }

    @Override
    public void deleteRow(String key) throws SQLException {
        super.executeSqlStatement("delete from \"18207_MATEYUK\".SUBCONTRACTORS where SUBCONTRACTOR_ID =" + Integer.parseInt(key));
    }

    @Override
    public void insertDefaultData() {
        super.executeSqlStatement("insert all\n" +
                "INTO \"18207_MATEYUK\".SUBCONTRACTORS (company, address, manager, price, phone) VALUES ('WEIGHT AND CO', 'Москва, ул.Длинная, 9', 'Могучий Алексей Витальевич', 35000, '89995550715')\n" +
                "SELECT * FROM dual");
    }

    @Override
    public ArrayList<String[]> getAllData() {
        ArrayList<String[]> rows = new ArrayList<>();
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery("SELECT * from \"18207_MATEYUK\".SUBCONTRACTORS");
            while (resultSet.next()) {
                String[] row = {
                        resultSet.getString(1),
                        resultSet.getString(2),
                        resultSet.getString(3),
                        resultSet.getString(4),
                        resultSet.getString(5),
                        resultSet.getString(6)
                };
                rows.add(row);
            }
            resultSet.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rows;
    }

    @Override
    public void insertRow(String[] row) throws SQLException {
        String sql = "INSERT INTO \"18207_MATEYUK\".SUBCONTRACTORS (company, address, manager, price, phone) VALUES (?, ?, ?, ?, ?)";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, row[0]);
        statement.setString(2, row[1]);
        statement.setString(3, row[2]);
        statement.setInt(4, Integer.parseInt(row[3]));
        statement.setString(5, row[4]);
        statement.executeUpdate();
        statement.close();
    }

}
