package com.qvv3r7y.repository.entity;

import com.qvv3r7y.repository.BaseTable;
import com.qvv3r7y.repository.TableOperations;

import java.sql.*;
import java.util.ArrayList;

public class WorkersEngineer extends BaseTable implements TableOperations {
    public WorkersEngineer(Connection connection) throws SQLException {
        super("workers_engineer", connection);
    }

    @Override
    public void createTable() throws SQLException {
        super.executeSqlStatement("CREATE TABLE \"18207_MATEYUK\".workers_engineer\n" +
                "(\n" +
                "    workers_id      NUMBER(6) PRIMARY KEY,\n" +
                "    academic_degree VARCHAR2(50) NULL,\n" +
                "    experience      NUMBER(2)    NOT NULL,\n" +
                "    number_patents  NUMBER(3)    NOT NULL,\n" +
                "    specialization  VARCHAR2(50) NULL,\n" +
                "    FOREIGN KEY (workers_id)\n" +
                "        REFERENCES workers (workers_id)\n" +
                ")", "Create table " + tableName);
    }

    @Override
    public void deleteTable() {
        super.executeSqlStatement("drop table \"18207_MATEYUK\".WORKERS_ENGINEER CASCADE CONSTRAINTS");
    }

    @Override
    public void deleteAllData() {
        super.executeSqlStatement("delete from \"18207_MATEYUK\".WORKERS_ENGINEER");
    }

    @Override
    public void deleteRow(String key) throws SQLException {
        super.executeSqlStatement("delete from \"18207_MATEYUK\".WORKERS_ENGINEER where WORKERS_ID =" + Integer.parseInt(key));
    }

    @Override
    public void insertDefaultData() {
        super.executeSqlStatement("INSERT ALL\n" +
                "into \"18207_MATEYUK\".WORKERS_ENGINEER (workers_id, academic_degree, experience, number_patents, specialization) VALUES (1, 'phD', 6, 10, 'proccesors')\n" +
                "into \"18207_MATEYUK\".WORKERS_ENGINEER (workers_id, academic_degree, experience, number_patents, specialization) VALUES (2, 'docent', 5, 8, 'phisics')\n" +
                "into \"18207_MATEYUK\".WORKERS_ENGINEER (workers_id, academic_degree, experience, number_patents, specialization) VALUES (3, 'science worker',3, 12, 'math')\n" +
                "into \"18207_MATEYUK\".WORKERS_ENGINEER (workers_id, academic_degree, experience, number_patents, specialization) VALUES (4, 'professor', 1, 0, 'IT')\n" +
                "SELECT * FROM dual");
    }

    @Override
    public ArrayList<String[]> getAllData() {

        ArrayList<String[]> rows = new ArrayList<>();
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery("SELECT * from \"18207_MATEYUK\".WORKERS_ENGINEER");
            while (resultSet.next()) {
                String[] row = {
                        resultSet.getString(1),
                        resultSet.getString(2),
                        resultSet.getString(3),
                        resultSet.getString(4),
                        resultSet.getString(5)
                };
                rows.add(row);
            }
            resultSet.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rows;
    }

    @Override
    public void insertRow(String[] row) throws SQLException {
        String sql = "INSERT into \"18207_MATEYUK\".WORKERS_ENGINEER (workers_id, academic_degree, experience, number_patents, specialization) VALUES VALUES (?,?,?,?,?)";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setInt(1, Integer.parseInt(row[0]));
        statement.setString(2, row[1]);
        statement.setInt(3, Integer.parseInt(row[2]));
        statement.setInt(4, Integer.parseInt(row[3]));
        statement.setString(5, row[4]);
        statement.executeUpdate();
        statement.close();
    }

}