package com.qvv3r7y.repository.entity;

import com.qvv3r7y.repository.BaseTable;
import com.qvv3r7y.repository.TableOperations;

import java.sql.*;
import java.util.ArrayList;
import java.util.Locale;

public class Clients extends BaseTable implements TableOperations {
    public Clients(Connection connection) throws SQLException {
        super("clients", connection);
    }

    @Override
    public void createTable() throws SQLException {
        super.executeSqlStatement("CREATE TABLE \"18207_MATEYUK\".clients\n" +
                "(\n" +
                "    client_id number(6) PRIMARY KEY,\n" +
                "    company   varchar2(100) NOT NULL,\n" +
                "    address   varchar2(100) NOT NULL,\n" +
                "    person    varchar2(100) NOT NULL,\n" +
                "    phone     varchar2(20)\n" +
                ")", "Create table " + tableName);
        super.executeSqlStatement("begin CREATE_AUTOINCREMENT('clients', 'client_id'); end;");
    }

    @Override
    public void deleteTable() {
        super.executeSqlStatement("drop table \"18207_MATEYUK\".CLIENTS CASCADE CONSTRAINTS");
        super.executeSqlStatement("drop sequence \"18207_MATEYUK\"." + tableName.toUpperCase(Locale.ROOT) + "SEQUENCE");
    }

    @Override
    public void deleteAllData() {
        super.executeSqlStatement("delete from \"18207_MATEYUK\".CLIENTS");
    }

    @Override
    public void deleteRow(String key) throws SQLException {
        super.executeSqlStatement("delete from \"18207_MATEYUK\".CLIENTS where CLIENT_ID =" + Integer.parseInt(key));
    }
    @Override
    public void insertDefaultData() {
        super.executeSqlStatement("insert all\n" +
                "into \"18207_MATEYUK\".CLIENTS (company, address, person, phone) VALUES ('ООО РОГА И КОПЫТА','г.Москва, ул.Вязов, 50','Иванов Иван Петрович',89002003010)\n" +
                "into \"18207_MATEYUK\".CLIENTS (company, address, person, phone) VALUES ('ОАО ВЕКТОР','г.Новосибирск, ул.Строителей, 32','Петров Петр Иванович',89961053515)\n" +
                "SELECT * FROM dual");
    }

    @Override
    public ArrayList<String[]> getAllData() {
        ArrayList<String[]> rows = new ArrayList<>();
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery("SELECT * from \"18207_MATEYUK\".clients");
            while (resultSet.next()) {
                String[] row = {
                        resultSet.getString(1),
                        resultSet.getString(2),
                        resultSet.getString(3),
                        resultSet.getString(4),
                        resultSet.getString(5)
                };
                rows.add(row);
            }
            resultSet.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rows;
    }

    @Override
    public void insertRow(String[] row) throws SQLException {
        String sql = "INSERT INTO \"18207_MATEYUK\".clients (company, address, person, phone) VALUES (?, ?, ?, ?)";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, row[0]);
        statement.setString(2, row[1]);
        statement.setString(3, row[2]);
        statement.setString(4, row[3]);
        statement.executeUpdate();
        statement.close();
    }

}
