package com.qvv3r7y.repository.entity;

import com.qvv3r7y.repository.BaseTable;
import com.qvv3r7y.repository.TableOperations;

import java.sql.*;
import java.util.ArrayList;
import java.util.Locale;

public class Positions extends BaseTable implements TableOperations {
    public Positions(Connection connection) throws SQLException {
        super("positions", connection);
    }

    @Override
    public void createTable() throws SQLException {
        super.executeSqlStatement("CREATE TABLE \"18207_MATEYUK\".positions\n" +
                "(\n" +
                "    position_id    NUMBER(6) PRIMARY KEY,\n" +
                "    position_title VARCHAR2(50) NOT NULL\n" +
                ")", "Create table " + tableName);
        super.executeSqlStatement("begin CREATE_AUTOINCREMENT('positions', 'position_id'); end;");
    }

    @Override
    public void deleteTable() {
        super.executeSqlStatement("drop table \"18207_MATEYUK\".POSITIONS CASCADE CONSTRAINTS");
        super.executeSqlStatement("drop sequence \"18207_MATEYUK\"." + tableName.toUpperCase(Locale.ROOT) + "SEQUENCE");
    }

    @Override
    public void deleteAllData() {
        super.executeSqlStatement("delete from \"18207_MATEYUK\".positions");
    }

    @Override
    public void deleteRow(String key) throws SQLException {
        super.executeSqlStatement("delete from \"18207_MATEYUK\".positions where POSITION_ID =" + Integer.parseInt(key));
    }

    @Override
    public void insertDefaultData() {
        super.executeSqlStatement("insert all\n" +
                " into \"18207_MATEYUK\".POSITIONS (position_id, position_title) VALUES (1,'designer')\n" +
                " into \"18207_MATEYUK\".POSITIONS (position_id, position_title) VALUES (2,'engineer')\n" +
                " into \"18207_MATEYUK\".POSITIONS (position_id, position_title) VALUES (3,'technician')\n" +
                " into \"18207_MATEYUK\".POSITIONS (position_id, position_title) VALUES (4,'laboratory assistant')\n" +
                " into \"18207_MATEYUK\".POSITIONS (position_id, position_title) VALUES (5,'marketer')\n" +
                " into \"18207_MATEYUK\".POSITIONS (position_id, position_title) VALUES (6,'analyst')\n" +
                "SELECT * FROM dual");
    }

    @Override
    public ArrayList<String[]> getAllData() {

        ArrayList<String[]> rows = new ArrayList<>();
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery("SELECT * from \"18207_MATEYUK\".positions");

            while (resultSet.next()) {
                String[] row = {
                        resultSet.getString(1),
                        resultSet.getString(2)
                };
                rows.add(row);
            }
            resultSet.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rows;
    }

    @Override
    public void insertRow(String[] row) throws SQLException {
        String sql = "INSERT INTO \"18207_MATEYUK\".POSITIONS (position_title) VALUES (?)";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, row[0]);
        statement.executeUpdate();
        statement.close();
    }

}