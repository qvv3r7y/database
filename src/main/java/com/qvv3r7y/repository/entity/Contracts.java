package com.qvv3r7y.repository.entity;

import com.qvv3r7y.repository.BaseTable;
import com.qvv3r7y.repository.TableOperations;

import java.sql.*;
import java.util.ArrayList;
import java.util.Locale;

public class Contracts extends BaseTable implements TableOperations {
    public Contracts(Connection connection) throws SQLException {
        super("contracts", connection);
    }

    @Override
    public void createTable() throws SQLException {
        super.executeSqlStatement("CREATE TABLE \"18207_MATEYUK\".contracts\n" +
                "(\n" +
                "    contract_id  number(6) PRIMARY KEY,\n" +
                "    bank_details number(20),\n" +
                "    conclusions  date NOT NULL,\n" +
                "    begin        date NOT NULL,\n" +
                "    finish       date,\n" +
                "    client_id    number(6),\n" +
                "    manager_id   number(6),\n" +
                "    CHECK (conclusions <= begin),\n" +
                "    CHECK ( finish is null or finish > begin),\n" +
                "    CONSTRAINT con_chief_fk\n" +
                "        FOREIGN KEY (manager_id)\n" +
                "            REFERENCES workers (workers_id),\n" +
                "    CONSTRAINT con_id_fk\n" +
                "        FOREIGN KEY (client_id)\n" +
                "            REFERENCES clients\n" +
                ")", "Create table " + tableName);
        super.executeSqlStatement("begin CREATE_AUTOINCREMENT('contracts', 'contract_id'); end;");
    }

    @Override
    public void deleteTable() {
        super.executeSqlStatement("drop table \"18207_MATEYUK\".CONTRACTS CASCADE CONSTRAINTS");
        super.executeSqlStatement("drop sequence \"18207_MATEYUK\"." + tableName.toUpperCase(Locale.ROOT) + "SEQUENCE");
    }

    @Override
    public void deleteAllData() {
        super.executeSqlStatement("delete from \"18207_MATEYUK\".CONTRACTS");
    }

    @Override
    public void deleteRow(String key) throws SQLException {
        super.executeSqlStatement("delete from \"18207_MATEYUK\".CONTRACTS where CONTRACT_ID =" + Integer.parseInt(key));
    }

    @Override
    public void insertDefaultData() {
        super.executeSqlStatement("insert all\n" +
                " INTO \"18207_MATEYUK\".CONTRACTS (bank_details, conclusions, begin, finish, client_id, manager_id) VALUES (9120939024934,'20/05/2020','01/09/2020','03/04/2021',1,4)\n" +
                "INTO \"18207_MATEYUK\".CONTRACTS (bank_details, conclusions, begin, finish, client_id, manager_id) VALUES (8265020574832,'10/01/2021','01/02/2021',NULL,2,1)\n" +
                "SELECT * FROM dual");
    }

    @Override
    public ArrayList<String[]> getAllData() {

        ArrayList<String[]> rows = new ArrayList<>();
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery("SELECT * from \"18207_MATEYUK\".contracts");
            while (resultSet.next()) {
                String[] row = {
                        resultSet.getString(1),
                        resultSet.getString(2),
                        resultSet.getString(3),
                        resultSet.getString(4),
                        resultSet.getString(5),
                        resultSet.getString(6),
                        resultSet.getString(7)
                };
                rows.add(row);
            }
            resultSet.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rows;
    }

    @Override
    public void insertRow(String[] row) throws SQLException {
        String sql = "INSERT INTO \"18207_MATEYUK\".CONTRACTS (bank_details, conclusions, begin, finish, client_id, manager_id) VALUES (?,?,?,?,?,?)";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setInt(1, Integer.parseInt(row[0]));
        statement.setDate(2, Date.valueOf(row[1]));
        statement.setDate(3, Date.valueOf(row[2]));
        try {
            statement.setDate(4, Date.valueOf(row[3]));
        } catch (IllegalArgumentException e) {
            statement.setDate(4, null);
        }
        statement.setInt(5, Integer.parseInt(row[4]));
        statement.setInt(6, Integer.parseInt(row[5]));
        statement.executeUpdate();
        statement.close();
    }
}
