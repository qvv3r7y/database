package com.qvv3r7y.repository.entity;

import com.qvv3r7y.repository.BaseTable;
import com.qvv3r7y.repository.TableOperations;

import java.sql.*;
import java.util.ArrayList;

public class WorkersManager extends BaseTable implements TableOperations {
    public WorkersManager(Connection connection) throws SQLException {
        super("workers_manager", connection);
    }

    @Override
    public void createTable() throws SQLException {
        super.executeSqlStatement("CREATE TABLE \"18207_MATEYUK\".workers_manager\n" +
                "(\n" +
                "    workers_id           NUMBER(6) PRIMARY KEY,\n" +
                "    efficiency           NUMBER(2, 1) NOT NULL,\n" +
                "    mentoring_experience NUMBER(2)    NOT NULL,\n" +
                "    access_level         NUMBER(1)    NOT NULL,\n" +
                "    FOREIGN KEY (workers_id)\n" +
                "        REFERENCES workers (workers_id)\n" +
                ")", "Create table " + tableName);
    }

    @Override
    public void deleteTable() {
        super.executeSqlStatement("drop table \"18207_MATEYUK\".WORKERS_MANAGER CASCADE CONSTRAINTS");
    }

    @Override
    public void deleteAllData() {
        super.executeSqlStatement("delete from \"18207_MATEYUK\".WORKERS_MANAGER");
    }

    @Override
    public void deleteRow(String key) throws SQLException {
        super.executeSqlStatement("delete from \"18207_MATEYUK\".WORKERS_MANAGER where WORKERS_ID =" + Integer.parseInt(key));
    }

    @Override
    public void insertDefaultData() {
        super.executeSqlStatement("INSERT ALL\n" +
                "into \"18207_MATEYUK\".WORKERS_MANAGER (workers_id, efficiency, mentoring_experience, access_level) VALUES (5, 8.77, 10, 3)\n" +
                "into \"18207_MATEYUK\".WORKERS_MANAGER (workers_id, efficiency, mentoring_experience, access_level) VALUES (6, 6.89, 4, 2)\n" +
                "into \"18207_MATEYUK\".WORKERS_MANAGER (workers_id, efficiency, mentoring_experience, access_level) VALUES (7, 4.36, 1, 1)\n" +
                "into \"18207_MATEYUK\".WORKERS_MANAGER (workers_id, efficiency, mentoring_experience, access_level) VALUES (8, 9.31, 15, 5)\n" +
                "SELECT * FROM dual");
    }

    @Override
    public ArrayList<String[]> getAllData() {

        ArrayList<String[]> rows = new ArrayList<>();
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery("SELECT * from \"18207_MATEYUK\".WORKERS_MANAGER");
            while (resultSet.next()) {
                String[] row = {
                        resultSet.getString(1),
                        resultSet.getString(2),
                        resultSet.getString(3),
                        resultSet.getString(4)
                };
                rows.add(row);
            }
            resultSet.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rows;
    }

    @Override
    public void insertRow(String[] row) throws SQLException {
        String sql = "INSERT into \"18207_MATEYUK\".WORKERS_MANAGER (workers_id, efficiency, mentoring_experience, access_level) VALUES VALUES (?,?,?,?)";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setInt(1, Integer.parseInt(row[0]));
        statement.setDouble(2, Double.parseDouble(row[1]));
        statement.setInt(3, Integer.parseInt(row[2]));
        statement.setInt(4, Integer.parseInt(row[3]));
        statement.executeUpdate();
        statement.close();
    }

}
