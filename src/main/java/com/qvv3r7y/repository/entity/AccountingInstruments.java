package com.qvv3r7y.repository.entity;

import com.qvv3r7y.repository.BaseTable;
import com.qvv3r7y.repository.TableOperations;

import java.sql.*;
import java.util.ArrayList;

public class AccountingInstruments extends BaseTable implements TableOperations {
    public AccountingInstruments(Connection connection) throws SQLException {
        super("accounting_instruments", connection);
    }

    @Override
    public void createTable() throws SQLException {
        super.executeSqlStatement("CREATE TABLE \"18207_MATEYUK\".accounting_instruments\n" +
                "(\n" +
                "    serial_id    number(6),\n" +
                "    project_abbr char(20),\n" +
                "    PRIMARY KEY (serial_id, project_abbr),\n" +
                "    FOREIGN KEY (project_abbr)\n" +
                "        REFERENCES projects (abbr)\n" +
                ")", "Create table " + tableName);
    }

    @Override
    public void deleteTable() {
        super.executeSqlStatement("drop table \"18207_MATEYUK\".ACCOUNTING_INSTRUMENTS CASCADE CONSTRAINTS");
    }

    @Override
    public void deleteAllData() {
        super.executeSqlStatement("delete from \"18207_MATEYUK\".ACCOUNTING_INSTRUMENTS");
    }

    @Override
    public void insertDefaultData() {
        super.executeSqlStatement("insert all\n" +
                "INTO \"18207_MATEYUK\".ACCOUNTING_INSTRUMENTS (serial_id, project_abbr) VALUES (1,'SUP_DOC')\n" +
                "SELECT * FROM dual");
    }

    @Override
    public ArrayList<String[]> getAllData() {
        ArrayList<String[]> rows = new ArrayList<>();
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery("SELECT * from \"18207_MATEYUK\".ACCOUNTING_INSTRUMENTS");
            while (resultSet.next()) {
                String[] row = {
                        resultSet.getString(1),
                        resultSet.getString(2)
                };
                rows.add(row);
            }
            resultSet.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rows;
    }

    @Override
    public void insertRow(String[] row) throws SQLException {
        String sql = "INSERT INTO \"18207_MATEYUK\".ACCOUNTING_INSTRUMENTS (serial_id, project_abbr) VALUES (?,?)";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setInt(1, Integer.parseInt(row[0]));
        statement.setString(2, row[1]);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void deleteRow(String key) throws SQLException {
        super.executeSqlStatement("delete from \"18207_MATEYUK\".ACCOUNTING_INSTRUMENTS where SERIAL_ID = " + Integer.parseInt(key));
    }

}