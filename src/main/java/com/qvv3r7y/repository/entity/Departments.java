package com.qvv3r7y.repository.entity;

import com.qvv3r7y.repository.BaseTable;
import com.qvv3r7y.repository.TableOperations;

import java.sql.*;
import java.util.ArrayList;
import java.util.Locale;

public class Departments extends BaseTable implements TableOperations {
    public Departments(Connection connection) throws SQLException {
        super("departments", connection);
    }

    @Override
    public void createTable() throws SQLException {
        super.executeSqlStatement("CREATE TABLE \"18207_MATEYUK\".departments\n" +
                "(\n" +
                "    department_id   NUMBER(6) PRIMARY KEY,\n" +
                "    department_name VARCHAR2(50) NOT NULL,\n" +
                "    chief_id        NUMBER(6)    NOT NULL\n" +
                ")", "Create table " + tableName);
        super.executeSqlStatement("begin CREATE_AUTOINCREMENT('departments', 'department_id'); end;");
    }

    @Override
    public void deleteTable() {
        super.executeSqlStatement("drop table \"18207_MATEYUK\".DEPARTMENTS CASCADE CONSTRAINTS");
        super.executeSqlStatement("drop sequence \"18207_MATEYUK\"." + tableName.toUpperCase(Locale.ROOT) + "SEQUENCE");
    }

    @Override
    public void deleteAllData() {
        super.executeSqlStatement("delete from \"18207_MATEYUK\".departments");
    }

    @Override
    public void deleteRow(String key) throws SQLException {
        super.executeSqlStatement("delete from \"18207_MATEYUK\".departments where DEPARTMENT_ID =" + Integer.parseInt(key));
    }

    @Override
    public void insertDefaultData() {
        super.executeSqlStatement("INSERT ALL\n" +
                " INTO \"18207_MATEYUK\".DEPARTMENTS (department_id, department_name, chief_id) VALUES (1,'Отдел Аналитики', 6)\n" +
                "INTO \"18207_MATEYUK\".DEPARTMENTS (department_id, department_name, chief_id) VALUES (2,'Отдел Маркетинга', 7)\n" +
                "INTO \"18207_MATEYUK\".DEPARTMENTS (department_id, department_name, chief_id) VALUES (3,'Отдел Разработки', 1)\n" +
                "INTO \"18207_MATEYUK\".DEPARTMENTS (department_id, department_name, chief_id) VALUES (4,'Отдел продаж', 4)\n" +
                "SELECT * FROM dual");
    }

    @Override
    public ArrayList<String[]> getAllData() {
        ArrayList<String[]> rows = new ArrayList<>();
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery("SELECT * from \"18207_MATEYUK\".departments");
            while (resultSet.next()) {
                String[] row = {
                        resultSet.getString(1),
                        resultSet.getString(2),
                        resultSet.getString(3)
                };
                rows.add(row);
            }
            resultSet.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rows;
    }

    @Override
    public void insertRow(String[] row) throws SQLException {
        String sql = "INSERT INTO \"18207_MATEYUK\".DEPARTMENTS (department_name, chief_id) VALUES (?, ?)";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, row[0]);
        statement.setInt(2, Integer.parseInt(row[1]));
        statement.executeUpdate();
        statement.close();
    }

}
