package com.qvv3r7y.repository.entity;

import com.qvv3r7y.repository.BaseTable;
import com.qvv3r7y.repository.TableOperations;

import java.sql.*;
import java.util.ArrayList;
import java.util.Locale;

public class Workers extends BaseTable implements TableOperations {
    public Workers(Connection connection) throws SQLException {
        super("workers", connection);
    }

    @Override
    public void createTable() throws SQLException {
        super.executeSqlStatement("CREATE TABLE \"18207_MATEYUK\".WORKERS\n" +
                "(\n" +
                "    workers_id    NUMBER(6) PRIMARY KEY,\n" +
                "    first_name    VARCHAR2(50),\n" +
                "    last_name     VARCHAR2(50) NOT NULL,\n" +
                "    phone_number  VARCHAR2(20) UNIQUE,\n" +
                "    date_of_birth DATE         NOT NULL,\n" +
                "    position_id   NUMBER(6)    NOT NULL,\n" +
                "    salary        NUMBER(8, 2),\n" +
                "    department_id NUMBER(6),\n" +
                "    type          VARCHAR2(50) NOT NULL,\n" +
                "    CONSTRAINT emp_salary_min\n" +
                "        CHECK (salary > 12500),\n" +
                "    CONSTRAINT birthdayConstraint\n" +
                "        CHECK (date_of_birth >= '01-jan-1900'),\n" +
                "    CONSTRAINT emp_job_fk\n" +
                "        FOREIGN KEY (position_id)\n" +
                "            REFERENCES positions,\n" +
                "    CONSTRAINT emp_dept_fk\n" +
                "        FOREIGN KEY (department_id)\n" +
                "            REFERENCES departments\n" +
                ")", "Create table " + tableName);
        super.executeSqlStatement("begin CREATE_AUTOINCREMENT('workers', 'workers_id'); end;");
    }

    @Override
    public void deleteTable() {
        super.executeSqlStatement("drop table \"18207_MATEYUK\".WORKERS CASCADE CONSTRAINTS");
        super.executeSqlStatement("drop sequence \"18207_MATEYUK\"." + tableName.toUpperCase(Locale.ROOT) + "SEQUENCE");
    }

    @Override
    public void deleteAllData() {
        super.executeSqlStatement("delete from \"18207_MATEYUK\".workers");
    }

    @Override
    public void deleteRow(String key) throws SQLException {
        super.executeSqlStatement("delete from \"18207_MATEYUK\".WORKERS where WORKERS_ID =" + Integer.parseInt(key));
    }

    @Override
    public void insertDefaultData() {
        super.executeSqlStatement("INSERT ALL\n" +
                "into \"18207_MATEYUK\".WORKERS (workers_id, first_name, last_name, phone_number, date_of_birth, position_id, salary, department_id, type) VALUES (1,'Илья','Матеюк',89002901010,'04.08.2000',2,80000,3, 'engineer')\n" +
                "into \"18207_MATEYUK\".WORKERS (workers_id, first_name, last_name, phone_number, date_of_birth, position_id, salary, department_id, type) VALUES (2,'Василий','Петров',89002909785,'04/04/2001',1,70000,3, 'engineer')\n" +
                "into \"18207_MATEYUK\".WORKERS (workers_id, first_name, last_name, phone_number, date_of_birth, position_id, salary, department_id, type) VALUES (3,'Петр','Сидоров',89002900530,'25/03/2003',3,60000,4, 'engineer')\n" +
                "into \"18207_MATEYUK\".WORKERS (workers_id, first_name, last_name, phone_number, date_of_birth, position_id, salary, department_id, type) VALUES (4,'Алиса','Синицын',89002142010,'24/05/2000',4,80000,4, 'engineer')\n" +
                "into \"18207_MATEYUK\".WORKERS (workers_id, first_name, last_name, phone_number, date_of_birth, position_id, salary, department_id, type) VALUES (5,'Данил','Павлов',89002793010,'14/10/2001',5,50000,2, 'manager')\n" +
                "into \"18207_MATEYUK\".WORKERS (workers_id, first_name, last_name, phone_number, date_of_birth, position_id, salary, department_id, type) VALUES (6,'Мария','Тищенко',89002902460,'24/11/2002',6,40000,1, 'manager')\n" +
                "into \"18207_MATEYUK\".WORKERS (workers_id, first_name, last_name, phone_number, date_of_birth, position_id, salary, department_id, type) VALUES (7,'Петр','Кот',89002901980,'08/12/2004',2,90000,4, 'manager')\n" +
                "into \"18207_MATEYUK\".WORKERS (workers_id, first_name, last_name, phone_number, date_of_birth, position_id, salary, department_id, type) VALUES (8,'Илья','Савин',89002761010,'12/09/2001',6,70000,1, 'manager')\n" +
                "SELECT * FROM dual");
    }

    @Override
    public ArrayList<String[]> getAllData() {

        ArrayList<String[]> rows = new ArrayList<>();
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery("SELECT * from \"18207_MATEYUK\".workers");
            while (resultSet.next()) {
                String[] row = {
                        resultSet.getString(1),
                        resultSet.getString(2),
                        resultSet.getString(3),
                        resultSet.getString(4),
                        resultSet.getString(5),
                        resultSet.getString(6),
                        resultSet.getString(7),
                        resultSet.getString(8),
                        resultSet.getString(9),
                };
                rows.add(row);
            }
            resultSet.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rows;
    }

    @Override
    public void insertRow(String[] row) throws SQLException {
        String sql = "INSERT INTO \"18207_MATEYUK\".WORKERS (first_name, last_name, phone_number, date_of_birth, position_id, salary, department_id, type) VALUES (?,?,?,?,?,?,?,?)";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, row[0]);
        statement.setString(2, row[1]);
        statement.setString(3, row[2]);
        statement.setDate(4, Date.valueOf(row[3]));
        statement.setInt(5, Integer.parseInt(row[4]));
        statement.setInt(6, Integer.parseInt(row[5]));
        statement.setInt(7, Integer.parseInt(row[6]));
        statement.setString(8, row[7]);
        statement.executeUpdate();
        statement.close();
    }

}
