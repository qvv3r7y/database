package com.qvv3r7y.repository.entity;

import com.qvv3r7y.repository.BaseTable;
import com.qvv3r7y.repository.TableOperations;

import java.sql.*;
import java.util.ArrayList;

public class ContractStages extends BaseTable implements TableOperations {
    public ContractStages(Connection connection) throws SQLException {
        super("contract_stages", connection);
    }

    @Override
    public void createTable() throws SQLException {
        super.executeSqlStatement("CREATE TABLE \"18207_MATEYUK\".contract_stages\n" +
                "(\n" +
                "    contract_id   number(6),\n" +
                "    project_abbr  char(20),\n" +
                "    subcontractor number(6) NULL,\n" +
                "    PRIMARY KEY (contract_id, project_abbr),\n" +
                "    FOREIGN KEY (contract_id)\n" +
                "        REFERENCES contracts (contract_id),\n" +
                "    FOREIGN KEY (project_abbr)\n" +
                "        REFERENCES projects (abbr),\n" +
                "    FOREIGN KEY (subcontractor)\n" +
                "        REFERENCES subcontractors (subcontractor_id)\n" +
                ")", "Create table " + tableName);
    }

    @Override
    public void deleteTable() {
        super.executeSqlStatement("drop table \"18207_MATEYUK\".CONTRACT_STAGES CASCADE CONSTRAINTS");
    }

    @Override
    public void deleteAllData() {
        super.executeSqlStatement("delete from \"18207_MATEYUK\".CONTRACT_STAGES");
    }

    @Override
    public void deleteRow(String key) throws SQLException {
        super.executeSqlStatement("delete from \"18207_MATEYUK\".CONTRACT_STAGES where CONTRACT_ID =" + Integer.parseInt(key));
    }

    @Override
    public void insertDefaultData() {
        super.executeSqlStatement("insert all\n" +
                "INTO \"18207_MATEYUK\".CONTRACT_STAGES (contract_id, project_abbr, subcontractor) VALUES (1,'DEV_DB', NULL)\n" +
                "INTO \"18207_MATEYUK\".CONTRACT_STAGES (contract_id, project_abbr, subcontractor) VALUES (2,'DEV_DOC', NULL)\n" +
                "INTO \"18207_MATEYUK\".CONTRACT_STAGES (contract_id, project_abbr, subcontractor) VALUES (2,'SUP_DOC', NULL)\n" +
                "SELECT * FROM dual");
    }

    @Override
    public ArrayList<String[]> getAllData() {

        ArrayList<String[]> rows = new ArrayList<>();
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery("SELECT * from \"18207_MATEYUK\".CONTRACT_STAGES");
            while (resultSet.next()) {
                String[] row = {
                        resultSet.getString(1),
                        resultSet.getString(2),
                        resultSet.getString(3)
                };
                rows.add(row);
            }
            resultSet.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rows;
    }

    @Override
    public void insertRow(String[] row) throws SQLException {
        String sql = "INSERT INTO \"18207_MATEYUK\".CONTRACT_STAGES (contract_id, project_abbr, subcontractor) VALUES (?,?,?)";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setInt(1, Integer.parseInt(row[0]));
        statement.setString(2, row[1]);
        statement.setInt(3, Integer.parseInt(row[2]));
        statement.executeUpdate();
        statement.close();
    }
}