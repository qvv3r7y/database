package com.qvv3r7y.repository.entity;

import com.qvv3r7y.repository.BaseTable;
import com.qvv3r7y.repository.TableOperations;

import java.sql.*;
import java.util.ArrayList;
import java.util.Locale;

public class Instruments extends BaseTable implements TableOperations {
    public Instruments(Connection connection) throws SQLException {
        super("instruments", connection);
    }

    @Override
    public void createTable() throws SQLException {
        super.executeSqlStatement("CREATE TABLE \"18207_MATEYUK\".instruments\n" +
                "(\n" +
                "    serial_id number(6) PRIMARY KEY,\n" +
                "    title     varchar2(50) NOT NULL,\n" +
                "    owner_id  number(6),\n" +
                "    FOREIGN KEY (owner_id)\n" +
                "        REFERENCES departments (department_id)\n" +
                ")", "Create table " + tableName);
        super.executeSqlStatement("begin CREATE_AUTOINCREMENT('instruments', 'serial_id'); end;");
    }

    @Override
    public void deleteTable() {
        super.executeSqlStatement("drop table \"18207_MATEYUK\".INSTRUMENTS CASCADE CONSTRAINTS");
        super.executeSqlStatement("drop sequence \"18207_MATEYUK\"." + tableName.toUpperCase(Locale.ROOT) + "SEQUENCE");
    }

    @Override
    public void deleteAllData() {
        super.executeSqlStatement("delete from \"18207_MATEYUK\".INSTRUMENTS");
    }

    @Override
    public void deleteRow(String key) throws SQLException {
        super.executeSqlStatement("delete from \"18207_MATEYUK\".INSTRUMENTS where SERIAL_ID =" + Integer.parseInt(key));
    }

    @Override
    public void insertDefaultData() {
        super.executeSqlStatement("insert all\n" +
                " INTO \"18207_MATEYUK\".INSTRUMENTS (title, owner_id) VALUES ('Ubuntu 20.0 2xi7 Server', 3)\n" +
                "INTO \"18207_MATEYUK\".INSTRUMENTS (title, owner_id) VALUES ('Windows 10 4xi5 Server', 2)\n" +
                "SELECT * FROM dual");
    }

    @Override
    public ArrayList<String[]> getAllData() {
        ArrayList<String[]> rows = new ArrayList<>();
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery("SELECT * from \"18207_MATEYUK\".instruments");
            while (resultSet.next()) {
                String[] row = {
                        resultSet.getString(1),
                        resultSet.getString(2),
                        resultSet.getString(3)
                };
                rows.add(row);
            }
            resultSet.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rows;
    }

    @Override
    public void insertRow(String[] row) throws SQLException {
        String sql = "INSERT INTO \"18207_MATEYUK\".INSTRUMENTS (title, owner_id) VALUES (?,?)";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, row[0]);
        statement.setInt(2, Integer.parseInt(row[1]));
        statement.executeUpdate();
        statement.close();
    }

}
