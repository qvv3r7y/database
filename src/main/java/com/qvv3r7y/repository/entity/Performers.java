package com.qvv3r7y.repository.entity;

import com.qvv3r7y.repository.BaseTable;
import com.qvv3r7y.repository.TableOperations;

import java.sql.*;
import java.util.ArrayList;

public class Performers extends BaseTable implements TableOperations {
    public Performers(Connection connection) throws SQLException {
        super("performers", connection);
    }

    @Override
    public void createTable() throws SQLException {
        super.executeSqlStatement("CREATE TABLE \"18207_MATEYUK\".performers\n" +
                "(\n" +
                "    project_abbr char(20),\n" +
                "    worker_id    number(6),\n" +
                "    role         varchar2(50) NOT NULL,\n" +
                "    PRIMARY KEY (project_abbr, worker_id),\n" +
                "    FOREIGN KEY (project_abbr)\n" +
                "        REFERENCES projects (abbr),\n" +
                "    FOREIGN KEY (worker_id)\n" +
                "        REFERENCES workers (workers_id)\n" +
                ")", "Create table " + tableName);
    }

    @Override
    public void deleteTable() {
        super.executeSqlStatement("drop table \"18207_MATEYUK\".PERFORMERS CASCADE CONSTRAINTS");
    }

    @Override
    public void deleteAllData() {
        super.executeSqlStatement("delete from \"18207_MATEYUK\".PERFORMERS");
    }

    @Override
    public void deleteRow(String key) throws SQLException {
        super.executeSqlStatement("delete from \"18207_MATEYUK\".PERFORMERS where PROJECT_ABBR =" + key);
    }

    @Override
    public void insertDefaultData() {
        super.executeSqlStatement("insert all\n" +
                "INTO \"18207_MATEYUK\".PERFORMERS (project_abbr, worker_id, role) VALUES ('DEV_DB',1, 'Разработка')\n" +
                "INTO \"18207_MATEYUK\".PERFORMERS (project_abbr, worker_id, role) VALUES ('DEV_DB',2, 'Тестирование')\n" +
                "INTO \"18207_MATEYUK\".PERFORMERS (project_abbr, worker_id, role) VALUES ('DEV_DB',5, 'Деплой')\n" +
                "INTO \"18207_MATEYUK\".PERFORMERS (project_abbr, worker_id, role) VALUES ('DEV_DOC',3,'Разработка')\n" +
                "INTO \"18207_MATEYUK\".PERFORMERS (project_abbr, worker_id, role) VALUES ('DEV_DOC',1, 'Разработка')\n" +
                "INTO \"18207_MATEYUK\".PERFORMERS (project_abbr, worker_id, role) VALUES ('SUP_DOC',4, 'Поддержка')\n" +
                "SELECT * FROM dual");
    }

    @Override
    public ArrayList<String[]> getAllData() {
        ArrayList<String[]> rows = new ArrayList<>();
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery("SELECT * from \"18207_MATEYUK\".performers");
            while (resultSet.next()) {
                String[] row = {
                        resultSet.getString(1),
                        resultSet.getString(2),
                        resultSet.getString(3)
                };
                rows.add(row);
            }
            resultSet.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rows;
    }

    @Override
    public void insertRow(String[] row) throws SQLException {
        String sql = "INSERT INTO \"18207_MATEYUK\".PERFORMERS (project_abbr, worker_id, role) VALUES (?,?,?)";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, row[0]);
        statement.setInt(2, Integer.parseInt(row[1]));
        statement.setString(3, row[2]);
        statement.executeUpdate();
        statement.close();
    }

}
