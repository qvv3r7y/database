package com.qvv3r7y.repository.entity;

import com.qvv3r7y.repository.BaseTable;
import com.qvv3r7y.repository.TableOperations;

import java.sql.*;
import java.util.ArrayList;
import java.util.Locale;

public class Projects extends BaseTable implements TableOperations {
    public Projects(Connection connection) throws SQLException {
        super("projects", connection);
    }

    @Override
    public void createTable() throws SQLException {
        super.executeSqlStatement("CREATE TABLE \"18207_MATEYUK\".projects\n" +
                "(\n" +
                "    project_id number(6)     NOT NULL UNIQUE,\n" +
                "    title      varchar2(120) NOT NULL,\n" +
                "    abbr       char(20) PRIMARY KEY,\n" +
                "    client_id  number(6),\n" +
                "    manager_id number(6),\n" +
                "    begin      date          NOT NULL,\n" +
                "    finish     date,\n" +
                "    price      number(10)    NOT NULL,\n" +
                "    CHECK ( price > 0 ),\n" +
                "    CHECK ( finish is null or finish > begin),\n" +
                "    CONSTRAINT cl_id_fk\n" +
                "        FOREIGN KEY (client_id)\n" +
                "            REFERENCES clients,\n" +
                "    CONSTRAINT emp_chief_fk\n" +
                "        FOREIGN KEY (manager_id)\n" +
                "            REFERENCES workers (workers_id)\n" +
                ")", "Create table " + tableName);
        super.executeSqlStatement("begin CREATE_AUTOINCREMENT('projects', 'project_id'); end;");
    }

    @Override
    public void deleteTable() {
        super.executeSqlStatement("drop table \"18207_MATEYUK\".PROJECTS CASCADE CONSTRAINTS");
        super.executeSqlStatement("drop sequence \"18207_MATEYUK\"." + tableName.toUpperCase(Locale.ROOT) + "SEQUENCE");
    }

    @Override
    public void deleteAllData() {
        super.executeSqlStatement("delete from \"18207_MATEYUK\".PROJECTS");
    }

    @Override
    public void deleteRow(String key) throws SQLException {
        super.executeSqlStatement("delete from \"18207_MATEYUK\".PROJECTS where ABBR =" + key);
    }

    @Override
    public void insertDefaultData() {
        super.executeSqlStatement("insert all\n" +
                " INTO \"18207_MATEYUK\".PROJECTS (title, abbr, client_id, manager_id, begin, finish, price) VALUES ('Разработка БД', 'DEV_DB',1,1,'01/09/2020','03/04/2021', 500000)\n" +
                "INTO \"18207_MATEYUK\".PROJECTS (title, abbr, client_id, manager_id, begin, finish, price) VALUES ('Разработка документооборота', 'DEV_DOC',2,5,'01/02/2021','03/04/2021', 100000)\n" +
                "INTO \"18207_MATEYUK\".PROJECTS (title, abbr, client_id, manager_id, begin, finish, price) VALUES ('Поддержка документооборота', 'SUP_DOC',2,5,'03/04/2021','03/05/2021', 500000)\n" +
                "SELECT * FROM dual");
    }

    @Override
    public ArrayList<String[]> getAllData() {

        ArrayList<String[]> rows = new ArrayList<>();
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery("SELECT * from \"18207_MATEYUK\".projects");
            while (resultSet.next()) {
                String[] row = {
                        resultSet.getString(1),
                        resultSet.getString(2),
                        resultSet.getString(3),
                        resultSet.getString(4),
                        resultSet.getString(5),
                        resultSet.getString(6),
                        resultSet.getString(7),
                        resultSet.getString(8)
                };
                rows.add(row);
            }
            resultSet.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rows;
    }

    @Override
    public void insertRow(String[] row) throws SQLException {
        String sql = "INSERT INTO \"18207_MATEYUK\".PROJECTS (title, abbr, client_id, manager_id, begin, finish, price) VALUES (?,?,?,?,?,?,?)";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, row[0]);
        statement.setString(2, row[1]);
        statement.setInt(3, Integer.parseInt(row[2]));
        statement.setInt(4, Integer.parseInt(row[3]));
        statement.setDate(5, Date.valueOf(row[4]));
        statement.setDate(6, Date.valueOf(row[5]));
        statement.setInt(7, Integer.parseInt(row[6]));
        statement.executeUpdate();
        statement.close();
    }
}
