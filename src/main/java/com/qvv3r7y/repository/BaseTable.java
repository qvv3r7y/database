package com.qvv3r7y.repository;

import java.io.Closeable;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

// Сервисный родительский класс, куда вынесена реализация общих действий для всех таблиц
public class BaseTable implements Closeable {
    protected Connection connection;  // JDBC-соединение для работы с таблицей
    protected String tableName;       // Имя таблицы

    public BaseTable(String tableName, Connection connection) { // Для реальной таблицы передадим в конструктор её имя
        this.tableName = tableName;
        this.connection = connection; // Установим соединение с СУБД для дальнейшей работы
    }

    // Закрытие
    public void close() {
        try {
            if (connection != null && !connection.isClosed())
                connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    // Выполнить SQL команду без параметров в СУБД, по завершению выдать сообщение в консоль
    public void executeSqlStatement(String sql, String description) throws SQLException {
        Statement statement = connection.createStatement();  // Создаем statement для выполнения sql-команд
        statement.execute(sql); // Выполняем statement - sql команду
        statement.close();      // Закрываем statement для фиксации изменений в СУБД
        if (description != null)
            System.out.println(description);
    }

    public void executeSqlStatement(String sql) {
        try {
            executeSqlStatement(sql, null);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<String[]> executeRequest(){
        return new ArrayList<>();
    }

    public String getTableName() {
        return tableName;
    }
}
