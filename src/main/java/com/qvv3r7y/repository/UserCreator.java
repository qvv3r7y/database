package com.qvv3r7y.repository;

import java.io.Closeable;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Savepoint;
import java.sql.Statement;

public class UserCreator implements Closeable {
    protected Connection connection;
    private boolean error = false;

    public UserCreator(Connection connection) {
        this.connection = connection;
    }

    public enum Roles {
        ADMIN("administrator_mateyuk"),
        HR("hr_mateyuk"),
        WORKER("workers_mateyuk"),
        CLIENT("client_mateyuk");

        private final String name;

        Roles(String name) {
            this.name = name();
        }

        public String getDBRole() {
            return name;
        }
    }

    // Закрытие
    public void close() {
        try {
            if (connection != null && !connection.isClosed())
                connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void createNewUser(String login, String password, Roles role) throws SQLException {
        Savepoint savepoint = null;
        try {
            connection.setAutoCommit(false);
            savepoint = connection.setSavepoint("before");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            executeSqlStatement("CREATE USER " + login + " IDENTIFIED BY " + password);
            executeSqlStatement("GRANT CONNECT, RESOURCE TO " + login);
            executeSqlStatement("GRANT " + role.getDBRole() + " TO " + login);
            connection.commit();
            System.out.println("User: " + login + " created successfully");
        } catch (SQLException err) {
            connection.rollback(savepoint);
            err.printStackTrace();
        }
        connection.setAutoCommit(true);
    }

    private void executeSqlStatement(String sql) throws SQLException {
        try (Statement statement = connection.createStatement()) {
            statement.execute(sql);
        }
    }

}
