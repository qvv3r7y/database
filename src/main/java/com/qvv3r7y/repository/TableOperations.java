package com.qvv3r7y.repository;

import java.sql.SQLException;
import java.util.ArrayList;

public interface TableOperations {
    void createTable() throws SQLException; // создание таблицы
    void deleteTable();
    void deleteAllData();
    void insertDefaultData();
    void insertRow(String[] row) throws SQLException;
    void deleteRow(String key) throws SQLException;
    ArrayList<String[]> getAllData();
}