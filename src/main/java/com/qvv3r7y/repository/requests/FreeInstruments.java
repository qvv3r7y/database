package com.qvv3r7y.repository.requests;

import com.qvv3r7y.repository.BaseTable;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class FreeInstruments extends BaseTable {
    public FreeInstruments(String tableName, Connection connection) {
        super(tableName, connection);
    }

    @Override
    public ArrayList<String[]> executeRequest() {
        ArrayList<String[]> rows = new ArrayList<>();
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery("select i.serial_id, i.title, d.department_name from \"18207_MATEYUK\".INSTRUMENTS i\n" +
                    "inner join \"18207_MATEYUK\".departments d\n" +
                    "on i.owner_id = d.department_id\n" +
                    "where i.serial_id NOT IN\n" +
                    "(SELECT a.serial_id from ACCOUNTING_INSTRUMENTS a)\n" +
                    "order by d.department_name");
            while (resultSet.next()) {
                String[] row = {
                        resultSet.getString(1),
                        resultSet.getString(2),
                        resultSet.getString(3)
                };
                rows.add(row);
            }
            resultSet.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rows;
    }
}