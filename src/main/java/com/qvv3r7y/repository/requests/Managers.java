package com.qvv3r7y.repository.requests;

import com.qvv3r7y.repository.BaseTable;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class Managers extends BaseTable {
    public Managers(String tableName, Connection connection)  {
        super(tableName, connection);
    }

    @Override
    public ArrayList<String[]> executeRequest() {
        ArrayList<String[]> rows = new ArrayList<>();
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery("SELECT w.workers_id, w.first_name, w.last_name, d.department_name from \"18207_MATEYUK\".WORKERS w\n" +
                    "inner join \"18207_MATEYUK\".departments d\n" +
                    "on w.workers_id = d.chief_id\n" +
                    "order by workers_id");
            while (resultSet.next()) {
                String[] row = {
                        resultSet.getString(1),
                        resultSet.getString(2),
                        resultSet.getString(3),
                        resultSet.getString(4)
                };
                rows.add(row);
            }
            resultSet.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rows;
    }
}